// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingGameModeBase() {}
// Cross Module References
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingGameModeBase_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameMode();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
// End Cross Module References
	static FName NAME_AMatchmakingGameModeBase_Server_LoginClient = FName(TEXT("Server_LoginClient"));
	void AMatchmakingGameModeBase::Server_LoginClient(APlayerController* NewPlayerController, const FString& Client_Name, const FString& Char_Name, const FString& Char_ID, const FString& Team, const FString& FullParameters)
	{
		MatchmakingGameModeBase_eventServer_LoginClient_Parms Parms;
		Parms.NewPlayerController=NewPlayerController;
		Parms.Client_Name=Client_Name;
		Parms.Char_Name=Char_Name;
		Parms.Char_ID=Char_ID;
		Parms.Team=Team;
		Parms.FullParameters=FullParameters;
		ProcessEvent(FindFunctionChecked(NAME_AMatchmakingGameModeBase_Server_LoginClient),&Parms);
	}
	void AMatchmakingGameModeBase::StaticRegisterNativesAMatchmakingGameModeBase()
	{
	}
	struct Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FullParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Team_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Team;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Char_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Char_ID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Char_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Char_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Client_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Client_Name;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewPlayerController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_FullParameters_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_FullParameters = { UE4CodeGen_Private::EPropertyClass::Str, "FullParameters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, FullParameters), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_FullParameters_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_FullParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Team_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Team = { UE4CodeGen_Private::EPropertyClass::Str, "Team", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, Team), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Team_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Team_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_ID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_ID = { UE4CodeGen_Private::EPropertyClass::Str, "Char_ID", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, Char_ID), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_ID_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_ID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_Name = { UE4CodeGen_Private::EPropertyClass::Str, "Char_Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, Char_Name), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_Name_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Client_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Client_Name = { UE4CodeGen_Private::EPropertyClass::Str, "Client_Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, Client_Name), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Client_Name_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Client_Name_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_NewPlayerController = { UE4CodeGen_Private::EPropertyClass::Object, "NewPlayerController", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingGameModeBase_eventServer_LoginClient_Parms, NewPlayerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_FullParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Team,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Char_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_Client_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::NewProp_NewPlayerController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "ModuleRelativePath", "Public/MatchmakingGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingGameModeBase, "Server_LoginClient", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(MatchmakingGameModeBase_eventServer_LoginClient_Parms), Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMatchmakingGameModeBase_NoRegister()
	{
		return AMatchmakingGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AMatchmakingGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMatchmakingGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameMode,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMatchmakingGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMatchmakingGameModeBase_Server_LoginClient, "Server_LoginClient" }, // 2310041030
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MatchmakingGameModeBase.h" },
		{ "ModuleRelativePath", "Public/MatchmakingGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMatchmakingGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMatchmakingGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMatchmakingGameModeBase_Statics::ClassParams = {
		&AMatchmakingGameModeBase::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002ACu,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMatchmakingGameModeBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMatchmakingGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMatchmakingGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMatchmakingGameModeBase, 2229593967);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMatchmakingGameModeBase(Z_Construct_UClass_AMatchmakingGameModeBase, &AMatchmakingGameModeBase::StaticClass, TEXT("/Script/Matchmaking"), TEXT("AMatchmakingGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMatchmakingGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
