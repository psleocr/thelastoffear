// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMatchmakingUDPComponent;
#ifdef MATCHMAKING_MatchmakingUDPComponent_generated_h
#error "MatchmakingUDPComponent.generated.h already included, missing '#pragma once' in MatchmakingUDPComponent.h"
#endif
#define MATCHMAKING_MatchmakingUDPComponent_generated_h

#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_9_DELEGATE \
struct _Script_Matchmaking_eventOnUDPReceive_Parms \
{ \
	UMatchmakingUDPComponent* cmp; \
	TArray<uint8> data; \
	int32 fromip; \
	int32 fromport; \
}; \
static inline void FOnUDPReceive_DelegateWrapper(const FMulticastScriptDelegate& OnUDPReceive, UMatchmakingUDPComponent* cmp, TArray<uint8> const& data, int32 fromip, int32 fromport) \
{ \
	_Script_Matchmaking_eventOnUDPReceive_Parms Parms; \
	Parms.cmp=cmp; \
	Parms.data=data; \
	Parms.fromip=fromip; \
	Parms.fromport=fromport; \
	OnUDPReceive.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execisSenderLocal) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->isSenderLocal(Z_Param_fromip,Z_Param_fromport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSendMessage) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data); \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_targetport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SendMessage(Z_Param_Out_data,Z_Param_targetip,Z_Param_targetport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSendAnswer) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SendAnswer(Z_Param_Out_data,Z_Param_fromip,Z_Param_fromport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStringToUTF8) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_data); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<uint8>*)Z_Param__Result=UMatchmakingUDPComponent::StringToUTF8(Z_Param_data); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUTF8ToString) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_utf8data); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UMatchmakingUDPComponent::UTF8ToString(Z_Param_Out_utf8data); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Stop(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStart) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_port); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Start(Z_Param_port); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execisSenderLocal) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->isSenderLocal(Z_Param_fromip,Z_Param_fromport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSendMessage) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data); \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_targetport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SendMessage(Z_Param_Out_data,Z_Param_targetip,Z_Param_targetport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSendAnswer) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromip); \
		P_GET_PROPERTY(UIntProperty,Z_Param_fromport); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->SendAnswer(Z_Param_Out_data,Z_Param_fromip,Z_Param_fromport); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStringToUTF8) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_data); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<uint8>*)Z_Param__Result=UMatchmakingUDPComponent::StringToUTF8(Z_Param_data); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUTF8ToString) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_utf8data); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=UMatchmakingUDPComponent::UTF8ToString(Z_Param_Out_utf8data); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Stop(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStart) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_port); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->Start(Z_Param_port); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMatchmakingUDPComponent(); \
	friend struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingUDPComponent, UActorComponent, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingUDPComponent)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMatchmakingUDPComponent(); \
	friend struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingUDPComponent, UActorComponent, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingUDPComponent)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingUDPComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingUDPComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingUDPComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingUDPComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingUDPComponent(UMatchmakingUDPComponent&&); \
	NO_API UMatchmakingUDPComponent(const UMatchmakingUDPComponent&); \
public:


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingUDPComponent(UMatchmakingUDPComponent&&); \
	NO_API UMatchmakingUDPComponent(const UMatchmakingUDPComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingUDPComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingUDPComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMatchmakingUDPComponent)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_14_PROLOG
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingUDPComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
