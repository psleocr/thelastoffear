// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingBPLibrary() {}
// Cross Module References
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingBPLibrary_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start();
// End Cross Module References
	void UMatchmakingBPLibrary::StaticRegisterNativesUMatchmakingBPLibrary()
	{
		UClass* Class = UMatchmakingBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Matchmaking_Client_Travel", &UMatchmakingBPLibrary::execMatchmaking_Client_Travel },
			{ "Matchmaking_DedicatedServer_ShutDown", &UMatchmakingBPLibrary::execMatchmaking_DedicatedServer_ShutDown },
			{ "Matchmaking_RunningInEditor", &UMatchmakingBPLibrary::execMatchmaking_RunningInEditor },
			{ "Matchmaking_Server_GetPort", &UMatchmakingBPLibrary::execMatchmaking_Server_GetPort },
			{ "Matchmaking_Server_Start", &UMatchmakingBPLibrary::execMatchmaking_Server_Start },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms
		{
			APlayerController* PlayerController;
			FString Adress;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Adress;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_Adress = { UE4CodeGen_Private::EPropertyClass::Str, "Adress", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms, Adress), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_PlayerController = { UE4CodeGen_Private::EPropertyClass::Object, "PlayerController", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms, PlayerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_Adress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_PlayerController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "DisplayName", "Travel to Dedicated Server" },
		{ "Keywords", "Matchmaking Travel to Dedicated Server" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Start a Dedicated Server on given Path with given Map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, "Matchmaking_Client_Travel", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "DisplayName", "Shut down Dedicated Server" },
		{ "Keywords", "Matchmaking Dedicated server shutdown" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Shut down the server (used for dedicated servers)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, "Matchmaking_DedicatedServer_ShutDown", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms), &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "DisplayName", "Is running in Editor" },
		{ "Keywords", "Matchmaking running in Editor" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "return True if the Project is running in the Editor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, "Matchmaking_RunningInEditor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms
		{
			AActor* SourceActor;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_SourceActor = { UE4CodeGen_Private::EPropertyClass::Object, "SourceActor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms, SourceActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_SourceActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "DisplayName", "Get used Server Port" },
		{ "Keywords", "Matchmaking Get used Server Port" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Get (if this is the Server) the Port the Server is running on" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, "Matchmaking_Server_GetPort", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms
		{
			FString ServerPath;
			FString Map;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Map;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_Map = { UE4CodeGen_Private::EPropertyClass::Str, "Map", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms, Map), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_ServerPath = { UE4CodeGen_Private::EPropertyClass::Str, "ServerPath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms, ServerPath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_Map,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_ServerPath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "DisplayName", "Start Dedicated Server" },
		{ "Keywords", "Matchmaking Start Dedicated Server" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Start a Dedicated Server on given Path with given Map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, "Matchmaking_Server_Start", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMatchmakingBPLibrary_NoRegister()
	{
		return UMatchmakingBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMatchmakingBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMatchmakingBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel, "Matchmaking_Client_Travel" }, // 3779552853
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown, "Matchmaking_DedicatedServer_ShutDown" }, // 989329185
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor, "Matchmaking_RunningInEditor" }, // 1108942143
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort, "Matchmaking_Server_GetPort" }, // 2606165481
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start, "Matchmaking_Server_Start" }, // 3986337912
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MatchmakingBPLibrary.h" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "*      Function library class.\n*      Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.\n*\n*      When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.\n*      BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.\n*      BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.\n*      DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.\n*                              Its lets you name the node using characters not allowed in C++ function names.\n*      CompactNodeTitle - the word(s) that appear on the node.\n*      Keywords -      the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.\n*                              Good example is \"Print String\" node which you can find also by using keyword \"log\".\n*      Category -      the category your node will be under in the Blueprint drop-down menu.\n*\n*      For more info on custom blueprint nodes visit documentation:\n*      https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMatchmakingBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMatchmakingBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMatchmakingBPLibrary_Statics::ClassParams = {
		&UMatchmakingBPLibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x000000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMatchmakingBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMatchmakingBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMatchmakingBPLibrary, 4242494320);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMatchmakingBPLibrary(Z_Construct_UClass_UMatchmakingBPLibrary, &UMatchmakingBPLibrary::StaticClass, TEXT("/Script/Matchmaking"), TEXT("UMatchmakingBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMatchmakingBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
