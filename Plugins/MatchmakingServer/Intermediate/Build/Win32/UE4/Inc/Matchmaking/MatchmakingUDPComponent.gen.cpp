// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingUDPComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingUDPComponent() {}
// Cross Module References
	MATCHMAKING_API UFunction* Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingUDPComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Start();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Stop();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics
	{
		struct _Script_Matchmaking_eventOnUDPReceive_Parms
		{
			UMatchmakingUDPComponent* cmp;
			TArray<uint8> data;
			int32 fromip;
			int32 fromport;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cmp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_cmp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromport = { UE4CodeGen_Private::EPropertyClass::Int, "fromport", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromip = { UE4CodeGen_Private::EPropertyClass::Int, "fromip", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data = { UE4CodeGen_Private::EPropertyClass::Array, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, data), METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData, ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp = { UE4CodeGen_Private::EPropertyClass::Object, "cmp", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, cmp), Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData, ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Matchmaking, "OnUDPReceive__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, sizeof(_Script_Matchmaking_eventOnUDPReceive_Parms), Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UMatchmakingUDPComponent::StaticRegisterNativesUMatchmakingUDPComponent()
	{
		UClass* Class = UMatchmakingUDPComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "isSenderLocal", &UMatchmakingUDPComponent::execisSenderLocal },
			{ "SendAnswer", &UMatchmakingUDPComponent::execSendAnswer },
			{ "SendMessage", &UMatchmakingUDPComponent::execSendMessage },
			{ "Start", &UMatchmakingUDPComponent::execStart },
			{ "Stop", &UMatchmakingUDPComponent::execStop },
			{ "StringToUTF8", &UMatchmakingUDPComponent::execStringToUTF8 },
			{ "UTF8ToString", &UMatchmakingUDPComponent::execUTF8ToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics
	{
		struct MatchmakingUDPComponent_eventisSenderLocal_Parms
		{
			int32 fromip;
			int32 fromport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventisSenderLocal_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingUDPComponent_eventisSenderLocal_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromport = { UE4CodeGen_Private::EPropertyClass::Int, "fromport", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventisSenderLocal_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromip = { UE4CodeGen_Private::EPropertyClass::Int, "fromip", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventisSenderLocal_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromip,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Check from where the data is coming" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "isSenderLocal", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(MatchmakingUDPComponent_eventisSenderLocal_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics
	{
		struct MatchmakingUDPComponent_eventSendAnswer_Parms
		{
			TArray<uint8> data;
			int32 fromip;
			int32 fromport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventSendAnswer_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingUDPComponent_eventSendAnswer_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromport = { UE4CodeGen_Private::EPropertyClass::Int, "fromport", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromip = { UE4CodeGen_Private::EPropertyClass::Int, "fromip", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data = { UE4CodeGen_Private::EPropertyClass::Array, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, data), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Send Data as Answer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "SendAnswer", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(MatchmakingUDPComponent_eventSendAnswer_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics
	{
		struct MatchmakingUDPComponent_eventSendMessage_Parms
		{
			TArray<uint8> data;
			FString targetip;
			int32 targetport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_targetport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_targetip_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_targetip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventSendMessage_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingUDPComponent_eventSendMessage_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetport = { UE4CodeGen_Private::EPropertyClass::Int, "targetport", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, targetport), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip = { UE4CodeGen_Private::EPropertyClass::Str, "targetip", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, targetip), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data = { UE4CodeGen_Private::EPropertyClass::Array, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, data), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Send Regular Data (String Adress)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "SendMessage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(MatchmakingUDPComponent_eventSendMessage_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics
	{
		struct MatchmakingUDPComponent_eventStart_Parms
		{
			int32 port;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_port;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventStart_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingUDPComponent_eventStart_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_port = { UE4CodeGen_Private::EPropertyClass::Int, "port", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventStart_Parms, port), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_port,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "CPP_Default_port", "19999" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Start the UDP Component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "Start", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(MatchmakingUDPComponent_eventStart_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Stop the UDP Component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "Stop", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Stop()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics
	{
		struct MatchmakingUDPComponent_eventStringToUTF8_Parms
		{
			FString data;
			TArray<uint8> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventStringToUTF8_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data = { UE4CodeGen_Private::EPropertyClass::Str, "data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventStringToUTF8_Parms, data), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Convert String -> UTF8" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "StringToUTF8", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(MatchmakingUDPComponent_eventStringToUTF8_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics
	{
		struct MatchmakingUDPComponent_eventUTF8ToString_Parms
		{
			TArray<uint8> utf8data;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_utf8data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_utf8data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_utf8data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventUTF8ToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data = { UE4CodeGen_Private::EPropertyClass::Array, "utf8data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(MatchmakingUDPComponent_eventUTF8ToString_Parms, utf8data), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "utf8data", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Convert UTF8 -> String" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, "UTF8ToString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(MatchmakingUDPComponent_eventUTF8ToString_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister()
	{
		return UMatchmakingUDPComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnUDPReceive_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnUDPReceive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMatchmakingUDPComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMatchmakingUDPComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal, "isSenderLocal" }, // 3340309483
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer, "SendAnswer" }, // 1201982112
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage, "SendMessage" }, // 1275375900
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_Start, "Start" }, // 4146527047
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_Stop, "Stop" }, // 3645029078
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8, "StringToUTF8" }, // 1763568926
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString, "UTF8ToString" }, // 1875960949
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Matchmaking" },
		{ "HideCategories", "Tags Activation Cooking Collision" },
		{ "IncludePath", "MatchmakingUDPComponent.h" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "UDP Tool for communication between Matchmaking-Server and Match-Server." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData[] = {
		{ "Category", "UDPCommand" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Event called when data is received" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "OnUDPReceive", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000010080000, 1, nullptr, STRUCT_OFFSET(UMatchmakingUDPComponent, OnUDPReceive), Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMatchmakingUDPComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMatchmakingUDPComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMatchmakingUDPComponent_Statics::ClassParams = {
		&UMatchmakingUDPComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMatchmakingUDPComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMatchmakingUDPComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMatchmakingUDPComponent, 1956864858);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMatchmakingUDPComponent(Z_Construct_UClass_UMatchmakingUDPComponent, &UMatchmakingUDPComponent::StaticClass, TEXT("/Script/Matchmaking"), TEXT("UMatchmakingUDPComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMatchmakingUDPComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
