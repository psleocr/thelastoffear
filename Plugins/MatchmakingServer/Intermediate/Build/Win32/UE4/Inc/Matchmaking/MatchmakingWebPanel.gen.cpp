// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingWebPanel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingWebPanel() {}
// Cross Module References
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingWebPanel_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingWebPanel();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters();
	MATCHMAKING_API UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath();
// End Cross Module References
	static FName NAME_AMatchmakingWebPanel_ProcessRequestParameters = FName(TEXT("ProcessRequestParameters"));
	FString AMatchmakingWebPanel::ProcessRequestParameters(const FString& RequestFileContents, TMap<FString,FString> const& Parameters, bool GetPost)
	{
		MatchmakingWebPanel_eventProcessRequestParameters_Parms Parms;
		Parms.RequestFileContents=RequestFileContents;
		Parms.Parameters=Parameters;
		Parms.GetPost=GetPost ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AMatchmakingWebPanel_ProcessRequestParameters),&Parms);
		return Parms.ReturnValue;
	}
	void AMatchmakingWebPanel::StaticRegisterNativesAMatchmakingWebPanel()
	{
		UClass* Class = AMatchmakingWebPanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMD5Sum", &AMatchmakingWebPanel::execGetMD5Sum },
			{ "ProcessRequestParameters", &AMatchmakingWebPanel::execProcessRequestParameters },
			{ "ValidatePath", &AMatchmakingWebPanel::execValidatePath },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics
	{
		struct MatchmakingWebPanel_eventGetMD5Sum_Parms
		{
			FString InputText;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventGetMD5Sum_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_InputText = { UE4CodeGen_Private::EPropertyClass::Str, "InputText", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventGetMD5Sum_Parms, InputText), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_InputText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, "GetMD5Sum", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(MatchmakingWebPanel_eventGetMD5Sum_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics
	{
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static void NewProp_GetPost_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GetPost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequestFileContents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RequestFileContents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost_SetBit(void* Obj)
	{
		((MatchmakingWebPanel_eventProcessRequestParameters_Parms*)Obj)->GetPost = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost = { UE4CodeGen_Private::EPropertyClass::Bool, "GetPost", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingWebPanel_eventProcessRequestParameters_Parms), &Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters = { UE4CodeGen_Private::EPropertyClass::Map, "Parameters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, Parameters), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Str, "Parameters_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_ValueProp = { UE4CodeGen_Private::EPropertyClass::Str, "Parameters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents = { UE4CodeGen_Private::EPropertyClass::Str, "RequestFileContents", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, RequestFileContents), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "BP Event for Parameters" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, "ProcessRequestParameters", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08420C00, sizeof(MatchmakingWebPanel_eventProcessRequestParameters_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics
	{
		struct MatchmakingWebPanel_eventValidatePath_Parms
		{
			FString Path;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingWebPanel_eventValidatePath_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MatchmakingWebPanel_eventValidatePath_Parms), &Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_Path = { UE4CodeGen_Private::EPropertyClass::Str, "Path", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MatchmakingWebPanel_eventValidatePath_Parms, Path), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_Path,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, "ValidatePath", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(MatchmakingWebPanel_eventValidatePath_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMatchmakingWebPanel_NoRegister()
	{
		return AMatchmakingWebPanel::StaticClass();
	}
	struct Z_Construct_UClass_AMatchmakingWebPanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePathRoot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePathRoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowDebugData_MetaData[];
#endif
		static void NewProp_ShowDebugData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ShowDebugData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ListenPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionAcceptDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ConnectionAcceptDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenTickDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ListenTickDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMatchmakingWebPanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMatchmakingWebPanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum, "GetMD5Sum" }, // 2847998674
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters, "ProcessRequestParameters" }, // 1529085465
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath, "ValidatePath" }, // 2854749204
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Matchmaking" },
		{ "HideCategories", "Tags Activation Cooking Collision" },
		{ "IncludePath", "MatchmakingWebPanel.h" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "WebPanel Actor for Matchmaking" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot = { UE4CodeGen_Private::EPropertyClass::Str, "FilePathRoot", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AMatchmakingWebPanel, FilePathRoot), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	void Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_SetBit(void* Obj)
	{
		((AMatchmakingWebPanel*)Obj)->ShowDebugData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData = { UE4CodeGen_Private::EPropertyClass::Bool, "ShowDebugData", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMatchmakingWebPanel), &Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort = { UE4CodeGen_Private::EPropertyClass::Int, "ListenPort", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AMatchmakingWebPanel, ListenPort), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay = { UE4CodeGen_Private::EPropertyClass::Float, "ConnectionAcceptDelay", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AMatchmakingWebPanel, ConnectionAcceptDelay), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "Public Vars" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration = { UE4CodeGen_Private::EPropertyClass::Float, "ListenTickDuration", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AMatchmakingWebPanel, ListenTickDuration), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMatchmakingWebPanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMatchmakingWebPanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::ClassParams = {
		&AMatchmakingWebPanel::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMatchmakingWebPanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMatchmakingWebPanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMatchmakingWebPanel, 1546078731);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMatchmakingWebPanel(Z_Construct_UClass_AMatchmakingWebPanel, &AMatchmakingWebPanel::StaticClass, TEXT("/Script/Matchmaking"), TEXT("AMatchmakingWebPanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMatchmakingWebPanel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
