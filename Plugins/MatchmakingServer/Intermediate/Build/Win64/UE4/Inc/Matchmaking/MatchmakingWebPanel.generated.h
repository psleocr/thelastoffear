// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
 
#ifdef MATCHMAKING_MatchmakingWebPanel_generated_h
#error "MatchmakingWebPanel.generated.h already included, missing '#pragma once' in MatchmakingWebPanel.h"
#endif
#define MATCHMAKING_MatchmakingWebPanel_generated_h

#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS \
	virtual FString ProcessRequestParameters_Implementation(const FString& RequestFileContents, TMap<FString,FString> const& Parameters, bool GetPost); \
 \
	DECLARE_FUNCTION(execProcessRequestParameters) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_RequestFileContents); \
		P_GET_TMAP_REF(FString,FString,Z_Param_Out_Parameters); \
		P_GET_UBOOL(Z_Param_GetPost); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->ProcessRequestParameters_Implementation(Z_Param_RequestFileContents,Z_Param_Out_Parameters,Z_Param_GetPost); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValidatePath) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Path); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ValidatePath(Z_Param_Path); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMD5Sum) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_InputText); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetMD5Sum(Z_Param_InputText); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execProcessRequestParameters) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_RequestFileContents); \
		P_GET_TMAP_REF(FString,FString,Z_Param_Out_Parameters); \
		P_GET_UBOOL(Z_Param_GetPost); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->ProcessRequestParameters_Implementation(Z_Param_RequestFileContents,Z_Param_Out_Parameters,Z_Param_GetPost); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValidatePath) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Path); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ValidatePath(Z_Param_Path); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMD5Sum) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_InputText); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetMD5Sum(Z_Param_InputText); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_EVENT_PARMS \
	struct MatchmakingWebPanel_eventProcessRequestParameters_Parms \
	{ \
		FString RequestFileContents; \
		TMap<FString,FString> Parameters; \
		bool GetPost; \
		FString ReturnValue; \
	};


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMatchmakingWebPanel(); \
	friend struct Z_Construct_UClass_AMatchmakingWebPanel_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingWebPanel, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingWebPanel)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS \
private: \
	static void StaticRegisterNativesAMatchmakingWebPanel(); \
	friend struct Z_Construct_UClass_AMatchmakingWebPanel_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingWebPanel, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingWebPanel)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMatchmakingWebPanel(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMatchmakingWebPanel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingWebPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingWebPanel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingWebPanel(AMatchmakingWebPanel&&); \
	NO_API AMatchmakingWebPanel(const AMatchmakingWebPanel&); \
public:


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingWebPanel(AMatchmakingWebPanel&&); \
	NO_API AMatchmakingWebPanel(const AMatchmakingWebPanel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingWebPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingWebPanel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMatchmakingWebPanel)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_24_PROLOG \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_EVENT_PARMS


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingWebPanel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
