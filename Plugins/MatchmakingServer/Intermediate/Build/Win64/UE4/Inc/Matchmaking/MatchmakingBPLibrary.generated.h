// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APlayerController;
#ifdef MATCHMAKING_MatchmakingBPLibrary_generated_h
#error "MatchmakingBPLibrary.generated.h already included, missing '#pragma once' in MatchmakingBPLibrary.h"
#endif
#define MATCHMAKING_MatchmakingBPLibrary_generated_h

#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMatchmaking_DedicatedServer_ShutDown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_DedicatedServer_ShutDown(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_RunningInEditor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_RunningInEditor(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Server_GetPort) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_SourceActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_Server_GetPort(Z_Param_SourceActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Client_Travel) \
	{ \
		P_GET_OBJECT(APlayerController,Z_Param_PlayerController); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Adress); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_Client_Travel(Z_Param_PlayerController,Z_Param_Adress); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Server_Start) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_ServerPath); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Map); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_Server_Start(Z_Param_ServerPath,Z_Param_Map); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMatchmaking_DedicatedServer_ShutDown) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_DedicatedServer_ShutDown(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_RunningInEditor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_RunningInEditor(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Server_GetPort) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_SourceActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_Server_GetPort(Z_Param_SourceActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Client_Travel) \
	{ \
		P_GET_OBJECT(APlayerController,Z_Param_PlayerController); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Adress); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_Client_Travel(Z_Param_PlayerController,Z_Param_Adress); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMatchmaking_Server_Start) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_ServerPath); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Map); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UMatchmakingBPLibrary::Matchmaking_Server_Start(Z_Param_ServerPath,Z_Param_Map); \
		P_NATIVE_END; \
	}


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMatchmakingBPLibrary(); \
	friend struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingBPLibrary)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUMatchmakingBPLibrary(); \
	friend struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingBPLibrary)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingBPLibrary(UMatchmakingBPLibrary&&); \
	NO_API UMatchmakingBPLibrary(const UMatchmakingBPLibrary&); \
public:


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingBPLibrary(UMatchmakingBPLibrary&&); \
	NO_API UMatchmakingBPLibrary(const UMatchmakingBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingBPLibrary)


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_27_PROLOG
#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS_NO_PURE_DECLS \
	Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MatchmakingBPLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Build___Portal_Dev_Marketplace_Full_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_Matchmaking_Source_Matchmaking_Public_MatchmakingBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
