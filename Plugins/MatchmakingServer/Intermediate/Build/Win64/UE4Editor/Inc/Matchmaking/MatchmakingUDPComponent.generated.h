// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMatchmakingUDPComponent;
#ifdef MATCHMAKING_MatchmakingUDPComponent_generated_h
#error "MatchmakingUDPComponent.generated.h already included, missing '#pragma once' in MatchmakingUDPComponent.h"
#endif
#define MATCHMAKING_MatchmakingUDPComponent_generated_h

#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_9_DELEGATE \
struct _Script_Matchmaking_eventOnUDPReceive_Parms \
{ \
	UMatchmakingUDPComponent* cmp; \
	TArray<uint8> data; \
	int32 fromip; \
	int32 fromport; \
}; \
static inline void FOnUDPReceive_DelegateWrapper(const FMulticastScriptDelegate& OnUDPReceive, UMatchmakingUDPComponent* cmp, TArray<uint8> const& data, int32 fromip, int32 fromport) \
{ \
	_Script_Matchmaking_eventOnUDPReceive_Parms Parms; \
	Parms.cmp=cmp; \
	Parms.data=data; \
	Parms.fromip=fromip; \
	Parms.fromport=fromport; \
	OnUDPReceive.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_SPARSE_DATA
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execisSenderLocal); \
	DECLARE_FUNCTION(execSendMessage); \
	DECLARE_FUNCTION(execSendAnswer); \
	DECLARE_FUNCTION(execStringToUTF8); \
	DECLARE_FUNCTION(execUTF8ToString); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execStart);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execisSenderLocal); \
	DECLARE_FUNCTION(execSendMessage); \
	DECLARE_FUNCTION(execSendAnswer); \
	DECLARE_FUNCTION(execStringToUTF8); \
	DECLARE_FUNCTION(execUTF8ToString); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execStart);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMatchmakingUDPComponent(); \
	friend struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingUDPComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingUDPComponent)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMatchmakingUDPComponent(); \
	friend struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingUDPComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingUDPComponent)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingUDPComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingUDPComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingUDPComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingUDPComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingUDPComponent(UMatchmakingUDPComponent&&); \
	NO_API UMatchmakingUDPComponent(const UMatchmakingUDPComponent&); \
public:


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingUDPComponent(UMatchmakingUDPComponent&&); \
	NO_API UMatchmakingUDPComponent(const UMatchmakingUDPComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingUDPComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingUDPComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMatchmakingUDPComponent)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_14_PROLOG
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCHMAKING_API UClass* StaticClass<class UMatchmakingUDPComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingUDPComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
