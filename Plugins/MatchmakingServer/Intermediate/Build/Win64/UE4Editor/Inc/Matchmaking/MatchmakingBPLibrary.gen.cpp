// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingBPLibrary() {}
// Cross Module References
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingBPLibrary_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMatchmakingBPLibrary::execMatchmaking_DedicatedServer_ShutDown)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UMatchmakingBPLibrary::Matchmaking_DedicatedServer_ShutDown();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingBPLibrary::execMatchmaking_RunningInEditor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_RunningInEditor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingBPLibrary::execMatchmaking_Server_GetPort)
	{
		P_GET_OBJECT(AActor,Z_Param_SourceActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UMatchmakingBPLibrary::Matchmaking_Server_GetPort(Z_Param_SourceActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingBPLibrary::execMatchmaking_Client_Travel)
	{
		P_GET_OBJECT(APlayerController,Z_Param_PlayerController);
		P_GET_PROPERTY(FStrProperty,Z_Param_Adress);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMatchmakingBPLibrary::Matchmaking_Client_Travel(Z_Param_PlayerController,Z_Param_Adress);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingBPLibrary::execMatchmaking_Server_Start)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ServerPath);
		P_GET_PROPERTY(FStrProperty,Z_Param_Map);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMatchmakingBPLibrary::Matchmaking_Server_Start(Z_Param_ServerPath,Z_Param_Map);
		P_NATIVE_END;
	}
	void UMatchmakingBPLibrary::StaticRegisterNativesUMatchmakingBPLibrary()
	{
		UClass* Class = UMatchmakingBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Matchmaking_Client_Travel", &UMatchmakingBPLibrary::execMatchmaking_Client_Travel },
			{ "Matchmaking_DedicatedServer_ShutDown", &UMatchmakingBPLibrary::execMatchmaking_DedicatedServer_ShutDown },
			{ "Matchmaking_RunningInEditor", &UMatchmakingBPLibrary::execMatchmaking_RunningInEditor },
			{ "Matchmaking_Server_GetPort", &UMatchmakingBPLibrary::execMatchmaking_Server_GetPort },
			{ "Matchmaking_Server_Start", &UMatchmakingBPLibrary::execMatchmaking_Server_Start },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms
		{
			APlayerController* PlayerController;
			FString Adress;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Adress;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_Adress = { "Adress", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms, Adress), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_PlayerController = { "PlayerController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms, PlayerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_Adress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::NewProp_PlayerController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "Comment", "// Start a Dedicated Server on given Path with given Map\n" },
		{ "DisplayName", "Travel to Dedicated Server" },
		{ "Keywords", "Matchmaking Travel to Dedicated Server" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Start a Dedicated Server on given Path with given Map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, nullptr, "Matchmaking_Client_Travel", nullptr, nullptr, sizeof(MatchmakingBPLibrary_eventMatchmaking_Client_Travel_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "Comment", "// Shut down the server (used for dedicated servers)\n" },
		{ "DisplayName", "Shut down Dedicated Server" },
		{ "Keywords", "Matchmaking Dedicated server shutdown" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Shut down the server (used for dedicated servers)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, nullptr, "Matchmaking_DedicatedServer_ShutDown", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms), &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "Comment", "// return True if the Project is running in the Editor\n" },
		{ "DisplayName", "Is running in Editor" },
		{ "Keywords", "Matchmaking running in Editor" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "return True if the Project is running in the Editor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, nullptr, "Matchmaking_RunningInEditor", nullptr, nullptr, sizeof(MatchmakingBPLibrary_eventMatchmaking_RunningInEditor_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms
		{
			AActor* SourceActor;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_SourceActor = { "SourceActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms, SourceActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::NewProp_SourceActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "Comment", "// Get (if this is the Server) the Port the Server is running on\n" },
		{ "DisplayName", "Get used Server Port" },
		{ "Keywords", "Matchmaking Get used Server Port" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Get (if this is the Server) the Port the Server is running on" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, nullptr, "Matchmaking_Server_GetPort", nullptr, nullptr, sizeof(MatchmakingBPLibrary_eventMatchmaking_Server_GetPort_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics
	{
		struct MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms
		{
			FString ServerPath;
			FString Map;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Map;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_Map = { "Map", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms, Map), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_ServerPath = { "ServerPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms, ServerPath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_Map,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::NewProp_ServerPath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking" },
		{ "Comment", "// Start a Dedicated Server on given Path with given Map\n" },
		{ "DisplayName", "Start Dedicated Server" },
		{ "Keywords", "Matchmaking Start Dedicated Server" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "Start a Dedicated Server on given Path with given Map" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingBPLibrary, nullptr, "Matchmaking_Server_Start", nullptr, nullptr, sizeof(MatchmakingBPLibrary_eventMatchmaking_Server_Start_Parms), Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMatchmakingBPLibrary_NoRegister()
	{
		return UMatchmakingBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMatchmakingBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMatchmakingBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Client_Travel, "Matchmaking_Client_Travel" }, // 114269083
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_DedicatedServer_ShutDown, "Matchmaking_DedicatedServer_ShutDown" }, // 1017547260
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_RunningInEditor, "Matchmaking_RunningInEditor" }, // 2628309343
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_GetPort, "Matchmaking_Server_GetPort" }, // 2722475267
		{ &Z_Construct_UFunction_UMatchmakingBPLibrary_Matchmaking_Server_Start, "Matchmaking_Server_Start" }, // 757807981
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* \n*\x09""Function library class.\n*\x09""Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.\n*\n*\x09When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.\n*\x09""BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.\n*\x09""BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.\n*\x09""DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.\n*\x09\x09\x09\x09Its lets you name the node using characters not allowed in C++ function names.\n*\x09""CompactNodeTitle - the word(s) that appear on the node.\n*\x09Keywords -\x09the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu. \n*\x09\x09\x09\x09Good example is \"Print String\" node which you can find also by using keyword \"log\".\n*\x09""Category -\x09the category your node will be under in the Blueprint drop-down menu.\n*\n*\x09""For more info on custom blueprint nodes visit documentation:\n*\x09https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation\n*/" },
		{ "IncludePath", "MatchmakingBPLibrary.h" },
		{ "ModuleRelativePath", "Public/MatchmakingBPLibrary.h" },
		{ "ToolTip", "*      Function library class.\n*      Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.\n*\n*      When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.\n*      BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.\n*      BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.\n*      DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.\n*                              Its lets you name the node using characters not allowed in C++ function names.\n*      CompactNodeTitle - the word(s) that appear on the node.\n*      Keywords -      the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.\n*                              Good example is \"Print String\" node which you can find also by using keyword \"log\".\n*      Category -      the category your node will be under in the Blueprint drop-down menu.\n*\n*      For more info on custom blueprint nodes visit documentation:\n*      https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMatchmakingBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMatchmakingBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMatchmakingBPLibrary_Statics::ClassParams = {
		&UMatchmakingBPLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMatchmakingBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMatchmakingBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMatchmakingBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMatchmakingBPLibrary, 38108969);
	template<> MATCHMAKING_API UClass* StaticClass<UMatchmakingBPLibrary>()
	{
		return UMatchmakingBPLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMatchmakingBPLibrary(Z_Construct_UClass_UMatchmakingBPLibrary, &UMatchmakingBPLibrary::StaticClass, TEXT("/Script/Matchmaking"), TEXT("UMatchmakingBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMatchmakingBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
