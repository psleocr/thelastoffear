// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
 
#ifdef MATCHMAKING_MatchmakingWebPanel_generated_h
#error "MatchmakingWebPanel.generated.h already included, missing '#pragma once' in MatchmakingWebPanel.h"
#endif
#define MATCHMAKING_MatchmakingWebPanel_generated_h

#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_SPARSE_DATA
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS \
	virtual FString ProcessRequestParameters_Implementation(const FString& RequestFileContents, TMap<FString,FString> const& Parameters, bool GetPost); \
 \
	DECLARE_FUNCTION(execProcessRequestParameters); \
	DECLARE_FUNCTION(execValidatePath); \
	DECLARE_FUNCTION(execGetMD5Sum);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execProcessRequestParameters); \
	DECLARE_FUNCTION(execValidatePath); \
	DECLARE_FUNCTION(execGetMD5Sum);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_EVENT_PARMS \
	struct MatchmakingWebPanel_eventProcessRequestParameters_Parms \
	{ \
		FString RequestFileContents; \
		TMap<FString,FString> Parameters; \
		bool GetPost; \
		FString ReturnValue; \
	};


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMatchmakingWebPanel(); \
	friend struct Z_Construct_UClass_AMatchmakingWebPanel_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingWebPanel, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingWebPanel)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS \
private: \
	static void StaticRegisterNativesAMatchmakingWebPanel(); \
	friend struct Z_Construct_UClass_AMatchmakingWebPanel_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingWebPanel, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingWebPanel)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMatchmakingWebPanel(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMatchmakingWebPanel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingWebPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingWebPanel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingWebPanel(AMatchmakingWebPanel&&); \
	NO_API AMatchmakingWebPanel(const AMatchmakingWebPanel&); \
public:


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingWebPanel(AMatchmakingWebPanel&&); \
	NO_API AMatchmakingWebPanel(const AMatchmakingWebPanel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingWebPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingWebPanel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMatchmakingWebPanel)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_24_PROLOG \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_EVENT_PARMS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_CALLBACK_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCHMAKING_API UClass* StaticClass<class AMatchmakingWebPanel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingWebPanel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
