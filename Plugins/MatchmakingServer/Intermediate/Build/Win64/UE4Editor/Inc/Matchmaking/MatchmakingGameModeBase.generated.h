// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APlayerController;
#ifdef MATCHMAKING_MatchmakingGameModeBase_generated_h
#error "MatchmakingGameModeBase.generated.h already included, missing '#pragma once' in MatchmakingGameModeBase.h"
#endif
#define MATCHMAKING_MatchmakingGameModeBase_generated_h

#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_SPARSE_DATA
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_RPC_WRAPPERS
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_EVENT_PARMS \
	struct MatchmakingGameModeBase_eventServer_LoginClient_Parms \
	{ \
		APlayerController* NewPlayerController; \
		FString Client_Name; \
		FString Char_Name; \
		FString Char_ID; \
		FString Team; \
		FString FullParameters; \
	};


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_CALLBACK_WRAPPERS
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMatchmakingGameModeBase(); \
	friend struct Z_Construct_UClass_AMatchmakingGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingGameModeBase, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingGameModeBase)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMatchmakingGameModeBase(); \
	friend struct Z_Construct_UClass_AMatchmakingGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMatchmakingGameModeBase, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(AMatchmakingGameModeBase)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMatchmakingGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMatchmakingGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingGameModeBase(AMatchmakingGameModeBase&&); \
	NO_API AMatchmakingGameModeBase(const AMatchmakingGameModeBase&); \
public:


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMatchmakingGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMatchmakingGameModeBase(AMatchmakingGameModeBase&&); \
	NO_API AMatchmakingGameModeBase(const AMatchmakingGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMatchmakingGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMatchmakingGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMatchmakingGameModeBase)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_13_PROLOG \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_EVENT_PARMS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_RPC_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_INCLASS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_CALLBACK_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCHMAKING_API UClass* StaticClass<class AMatchmakingGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
