// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingUDPComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingUDPComponent() {}
// Cross Module References
	MATCHMAKING_API UFunction* Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_UMatchmakingUDPComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics
	{
		struct _Script_Matchmaking_eventOnUDPReceive_Parms
		{
			UMatchmakingUDPComponent* cmp;
			TArray<uint8> data;
			int32 fromip;
			int32 fromport;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cmp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_cmp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromport = { "fromport", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromip = { "fromip", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp = { "cmp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Matchmaking_eventOnUDPReceive_Parms, cmp), Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_fromip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::NewProp_cmp,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Matchmaking, nullptr, "OnUDPReceive__DelegateSignature", nullptr, nullptr, sizeof(_Script_Matchmaking_eventOnUDPReceive_Parms), Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execisSenderLocal)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_fromip);
		P_GET_PROPERTY(FIntProperty,Z_Param_fromport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->isSenderLocal(Z_Param_fromip,Z_Param_fromport);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execSendMessage)
	{
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data);
		P_GET_PROPERTY(FStrProperty,Z_Param_targetip);
		P_GET_PROPERTY(FIntProperty,Z_Param_targetport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendMessage(Z_Param_Out_data,Z_Param_targetip,Z_Param_targetport);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execSendAnswer)
	{
		P_GET_TARRAY_REF(uint8,Z_Param_Out_data);
		P_GET_PROPERTY(FIntProperty,Z_Param_fromip);
		P_GET_PROPERTY(FIntProperty,Z_Param_fromport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendAnswer(Z_Param_Out_data,Z_Param_fromip,Z_Param_fromport);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execStringToUTF8)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_data);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<uint8>*)Z_Param__Result=UMatchmakingUDPComponent::StringToUTF8(Z_Param_data);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execUTF8ToString)
	{
		P_GET_TARRAY_REF(uint8,Z_Param_Out_utf8data);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UMatchmakingUDPComponent::UTF8ToString(Z_Param_Out_utf8data);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execStop)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Stop();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchmakingUDPComponent::execStart)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_port);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->Start(Z_Param_port);
		P_NATIVE_END;
	}
	void UMatchmakingUDPComponent::StaticRegisterNativesUMatchmakingUDPComponent()
	{
		UClass* Class = UMatchmakingUDPComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "isSenderLocal", &UMatchmakingUDPComponent::execisSenderLocal },
			{ "SendAnswer", &UMatchmakingUDPComponent::execSendAnswer },
			{ "SendMessage", &UMatchmakingUDPComponent::execSendMessage },
			{ "Start", &UMatchmakingUDPComponent::execStart },
			{ "Stop", &UMatchmakingUDPComponent::execStop },
			{ "StringToUTF8", &UMatchmakingUDPComponent::execStringToUTF8 },
			{ "UTF8ToString", &UMatchmakingUDPComponent::execUTF8ToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics
	{
		struct MatchmakingUDPComponent_eventisSenderLocal_Parms
		{
			int32 fromip;
			int32 fromport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventisSenderLocal_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingUDPComponent_eventisSenderLocal_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromport = { "fromport", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventisSenderLocal_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromip = { "fromip", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventisSenderLocal_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::NewProp_fromip,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Check from where the data is coming\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Check from where the data is coming" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "isSenderLocal", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventisSenderLocal_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics
	{
		struct MatchmakingUDPComponent_eventSendAnswer_Parms
		{
			TArray<uint8> data;
			int32 fromip;
			int32 fromport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromport;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_fromip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventSendAnswer_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingUDPComponent_eventSendAnswer_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromport = { "fromport", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, fromport), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromip = { "fromip", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, fromip), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendAnswer_Parms, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_fromip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::NewProp_data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Send Data as Answer\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Send Data as Answer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "SendAnswer", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventSendAnswer_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics
	{
		struct MatchmakingUDPComponent_eventSendMessage_Parms
		{
			TArray<uint8> data;
			FString targetip;
			int32 targetport;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_targetport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_targetip_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_targetip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventSendMessage_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingUDPComponent_eventSendMessage_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetport = { "targetport", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, targetport), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip = { "targetip", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, targetip), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventSendMessage_Parms, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_targetip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::NewProp_data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Send Regular Data (String Adress)\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Send Regular Data (String Adress)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "SendMessage", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventSendMessage_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics
	{
		struct MatchmakingUDPComponent_eventStart_Parms
		{
			int32 port;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_port;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingUDPComponent_eventStart_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingUDPComponent_eventStart_Parms), &Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_port = { "port", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventStart_Parms, port), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::NewProp_port,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Start the UDP Component\n" },
		{ "CPP_Default_port", "19999" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Start the UDP Component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "Start", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventStart_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Stop the UDP Component\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Stop the UDP Component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "Stop", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_Stop()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_Stop_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics
	{
		struct MatchmakingUDPComponent_eventStringToUTF8_Parms
		{
			FString data;
			TArray<uint8> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventStringToUTF8_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventStringToUTF8_Parms, data), METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::NewProp_data,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Convert String -> UTF8\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Convert String -> UTF8" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "StringToUTF8", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventStringToUTF8_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics
	{
		struct MatchmakingUDPComponent_eventUTF8ToString_Parms
		{
			TArray<uint8> utf8data;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_utf8data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_utf8data;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_utf8data_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventUTF8ToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data = { "utf8data", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingUDPComponent_eventUTF8ToString_Parms, utf8data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_Inner = { "utf8data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::NewProp_utf8data_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "// Convert UTF8 -> String\n" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Convert UTF8 -> String" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchmakingUDPComponent, nullptr, "UTF8ToString", nullptr, nullptr, sizeof(MatchmakingUDPComponent_eventUTF8ToString_Parms), Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMatchmakingUDPComponent_NoRegister()
	{
		return UMatchmakingUDPComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMatchmakingUDPComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnUDPReceive_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnUDPReceive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMatchmakingUDPComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMatchmakingUDPComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_isSenderLocal, "isSenderLocal" }, // 4113819465
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_SendAnswer, "SendAnswer" }, // 4284905051
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_SendMessage, "SendMessage" }, // 3229946947
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_Start, "Start" }, // 2993670911
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_Stop, "Stop" }, // 1204675504
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_StringToUTF8, "StringToUTF8" }, // 2356409594
		{ &Z_Construct_UFunction_UMatchmakingUDPComponent_UTF8ToString, "UTF8ToString" }, // 821163065
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Matchmaking" },
		{ "Comment", "/**\n* UDP Tool for communication between Matchmaking-Server and Match-Server.\n*/" },
		{ "HideCategories", "Tags Activation Cooking Collision" },
		{ "IncludePath", "MatchmakingUDPComponent.h" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "UDP Tool for communication between Matchmaking-Server and Match-Server." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData[] = {
		{ "Category", "UDPCommand" },
		{ "Comment", "/** Event called when data is received */" },
		{ "ModuleRelativePath", "Public/MatchmakingUDPComponent.h" },
		{ "ToolTip", "Event called when data is received" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive = { "OnUDPReceive", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMatchmakingUDPComponent, OnUDPReceive), Z_Construct_UDelegateFunction_Matchmaking_OnUDPReceive__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMatchmakingUDPComponent_Statics::NewProp_OnUDPReceive,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMatchmakingUDPComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMatchmakingUDPComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMatchmakingUDPComponent_Statics::ClassParams = {
		&UMatchmakingUDPComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMatchmakingUDPComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMatchmakingUDPComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMatchmakingUDPComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMatchmakingUDPComponent, 533500583);
	template<> MATCHMAKING_API UClass* StaticClass<UMatchmakingUDPComponent>()
	{
		return UMatchmakingUDPComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMatchmakingUDPComponent(Z_Construct_UClass_UMatchmakingUDPComponent, &UMatchmakingUDPComponent::StaticClass, TEXT("/Script/Matchmaking"), TEXT("UMatchmakingUDPComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMatchmakingUDPComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
