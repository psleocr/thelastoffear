// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APlayerController;
#ifdef MATCHMAKING_MatchmakingBPLibrary_generated_h
#error "MatchmakingBPLibrary.generated.h already included, missing '#pragma once' in MatchmakingBPLibrary.h"
#endif
#define MATCHMAKING_MatchmakingBPLibrary_generated_h

#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_SPARSE_DATA
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMatchmaking_DedicatedServer_ShutDown); \
	DECLARE_FUNCTION(execMatchmaking_RunningInEditor); \
	DECLARE_FUNCTION(execMatchmaking_Server_GetPort); \
	DECLARE_FUNCTION(execMatchmaking_Client_Travel); \
	DECLARE_FUNCTION(execMatchmaking_Server_Start);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMatchmaking_DedicatedServer_ShutDown); \
	DECLARE_FUNCTION(execMatchmaking_RunningInEditor); \
	DECLARE_FUNCTION(execMatchmaking_Server_GetPort); \
	DECLARE_FUNCTION(execMatchmaking_Client_Travel); \
	DECLARE_FUNCTION(execMatchmaking_Server_Start);


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMatchmakingBPLibrary(); \
	friend struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingBPLibrary)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUMatchmakingBPLibrary(); \
	friend struct Z_Construct_UClass_UMatchmakingBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UMatchmakingBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Matchmaking"), NO_API) \
	DECLARE_SERIALIZER(UMatchmakingBPLibrary)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingBPLibrary(UMatchmakingBPLibrary&&); \
	NO_API UMatchmakingBPLibrary(const UMatchmakingBPLibrary&); \
public:


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchmakingBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchmakingBPLibrary(UMatchmakingBPLibrary&&); \
	NO_API UMatchmakingBPLibrary(const UMatchmakingBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchmakingBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchmakingBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchmakingBPLibrary)


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_27_PROLOG
#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_SPARSE_DATA \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h_30_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MatchmakingBPLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCHMAKING_API UClass* StaticClass<class UMatchmakingBPLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Marketplace_MatchmakingServer_Source_Matchmaking_Public_MatchmakingBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
