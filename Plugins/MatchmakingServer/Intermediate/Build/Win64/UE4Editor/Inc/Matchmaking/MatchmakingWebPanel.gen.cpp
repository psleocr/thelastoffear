// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Matchmaking/Public/MatchmakingWebPanel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchmakingWebPanel() {}
// Cross Module References
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingWebPanel_NoRegister();
	MATCHMAKING_API UClass* Z_Construct_UClass_AMatchmakingWebPanel();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Matchmaking();
// End Cross Module References
	DEFINE_FUNCTION(AMatchmakingWebPanel::execProcessRequestParameters)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_RequestFileContents);
		P_GET_TMAP_REF(FString,FString,Z_Param_Out_Parameters);
		P_GET_UBOOL(Z_Param_GetPost);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->ProcessRequestParameters_Implementation(Z_Param_RequestFileContents,Z_Param_Out_Parameters,Z_Param_GetPost);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMatchmakingWebPanel::execValidatePath)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Path);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ValidatePath(Z_Param_Path);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMatchmakingWebPanel::execGetMD5Sum)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputText);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetMD5Sum(Z_Param_InputText);
		P_NATIVE_END;
	}
	static FName NAME_AMatchmakingWebPanel_ProcessRequestParameters = FName(TEXT("ProcessRequestParameters"));
	FString AMatchmakingWebPanel::ProcessRequestParameters(const FString& RequestFileContents, TMap<FString,FString> const& Parameters, bool GetPost)
	{
		MatchmakingWebPanel_eventProcessRequestParameters_Parms Parms;
		Parms.RequestFileContents=RequestFileContents;
		Parms.Parameters=Parameters;
		Parms.GetPost=GetPost ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AMatchmakingWebPanel_ProcessRequestParameters),&Parms);
		return Parms.ReturnValue;
	}
	void AMatchmakingWebPanel::StaticRegisterNativesAMatchmakingWebPanel()
	{
		UClass* Class = AMatchmakingWebPanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMD5Sum", &AMatchmakingWebPanel::execGetMD5Sum },
			{ "ProcessRequestParameters", &AMatchmakingWebPanel::execProcessRequestParameters },
			{ "ValidatePath", &AMatchmakingWebPanel::execValidatePath },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics
	{
		struct MatchmakingWebPanel_eventGetMD5Sum_Parms
		{
			FString InputText;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InputText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventGetMD5Sum_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_InputText = { "InputText", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventGetMD5Sum_Parms, InputText), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::NewProp_InputText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, nullptr, "GetMD5Sum", nullptr, nullptr, sizeof(MatchmakingWebPanel_eventGetMD5Sum_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics
	{
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static void NewProp_GetPost_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GetPost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequestFileContents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RequestFileContents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost_SetBit(void* Obj)
	{
		((MatchmakingWebPanel_eventProcessRequestParameters_Parms*)Obj)->GetPost = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost = { "GetPost", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingWebPanel_eventProcessRequestParameters_Parms), &Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, Parameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_Key_KeyProp = { "Parameters_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_ValueProp = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents = { "RequestFileContents", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventProcessRequestParameters_Parms, RequestFileContents), METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_GetPost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::NewProp_RequestFileContents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "Comment", "// BP Event for Parameters\n" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "BP Event for Parameters" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, nullptr, "ProcessRequestParameters", nullptr, nullptr, sizeof(MatchmakingWebPanel_eventProcessRequestParameters_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics
	{
		struct MatchmakingWebPanel_eventValidatePath_Parms
		{
			FString Path;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MatchmakingWebPanel_eventValidatePath_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MatchmakingWebPanel_eventValidatePath_Parms), &Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MatchmakingWebPanel_eventValidatePath_Parms, Path), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::NewProp_Path,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Matchmaking WebPanel" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMatchmakingWebPanel, nullptr, "ValidatePath", nullptr, nullptr, sizeof(MatchmakingWebPanel_eventValidatePath_Parms), Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMatchmakingWebPanel_NoRegister()
	{
		return AMatchmakingWebPanel::StaticClass();
	}
	struct Z_Construct_UClass_AMatchmakingWebPanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePathRoot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePathRoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowDebugData_MetaData[];
#endif
		static void NewProp_ShowDebugData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ShowDebugData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ListenPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionAcceptDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ConnectionAcceptDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenTickDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ListenTickDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMatchmakingWebPanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Matchmaking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMatchmakingWebPanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_GetMD5Sum, "GetMD5Sum" }, // 166263410
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_ProcessRequestParameters, "ProcessRequestParameters" }, // 1664655005
		{ &Z_Construct_UFunction_AMatchmakingWebPanel_ValidatePath, "ValidatePath" }, // 3047522416
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Matchmaking" },
		{ "Comment", "/**\n* WebPanel Actor for Matchmaking\n*/" },
		{ "HideCategories", "Tags Activation Cooking Collision" },
		{ "IncludePath", "MatchmakingWebPanel.h" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "WebPanel Actor for Matchmaking" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot = { "FilePathRoot", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMatchmakingWebPanel, FilePathRoot), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	void Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_SetBit(void* Obj)
	{
		((AMatchmakingWebPanel*)Obj)->ShowDebugData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData = { "ShowDebugData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMatchmakingWebPanel), &Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort = { "ListenPort", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMatchmakingWebPanel, ListenPort), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay = { "ConnectionAcceptDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMatchmakingWebPanel, ConnectionAcceptDelay), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Public Vars\n" },
		{ "ModuleRelativePath", "Public/MatchmakingWebPanel.h" },
		{ "ToolTip", "Public Vars" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration = { "ListenTickDuration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMatchmakingWebPanel, ListenTickDuration), METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_FilePathRoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ShowDebugData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ConnectionAcceptDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMatchmakingWebPanel_Statics::NewProp_ListenTickDuration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMatchmakingWebPanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMatchmakingWebPanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMatchmakingWebPanel_Statics::ClassParams = {
		&AMatchmakingWebPanel::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMatchmakingWebPanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMatchmakingWebPanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMatchmakingWebPanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMatchmakingWebPanel, 3226179589);
	template<> MATCHMAKING_API UClass* StaticClass<AMatchmakingWebPanel>()
	{
		return AMatchmakingWebPanel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMatchmakingWebPanel(Z_Construct_UClass_AMatchmakingWebPanel, &AMatchmakingWebPanel::StaticClass, TEXT("/Script/Matchmaking"), TEXT("AMatchmakingWebPanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMatchmakingWebPanel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
