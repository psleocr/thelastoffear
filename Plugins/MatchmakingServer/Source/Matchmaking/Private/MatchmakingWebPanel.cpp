// Copyright 2017-2018 Robin Zinser. All Rights Reserved.

#include "MatchmakingWebPanel.h"
#include "HAL/RunnableThread.h"
#include "HAL/Runnable.h"


// FTCPListenThread
//
// Task: Thread for handeling TCP Web Communication.
//
class FTCPListenThread : public FRunnable
{
	// ================= //
	//     Variables     //
	// ================= //
private:
	AMatchmakingWebPanel* _cmp;
	FRunnableThread* Thread;
	volatile bool bexit;

public:

	// Constructor()
	//
	// Task: Create actual thread and receive TCPActor ref
	//
	FTCPListenThread(AMatchmakingWebPanel* cmp) : _cmp(cmp), bexit(false)
	{
		Thread = FRunnableThread::Create(this, TEXT("FTCPListenThread"), 0, TPri_AboveNormal);
	}
	// Constructor()



	// Stop()
	//
	// Task: Stop the Thread
	//
	virtual void Stop()
	{
		bexit = true;
		Thread->WaitForCompletion();
	}
	// Stop()



	// Run()
	//
	// Task: Run the actual Listen Socket as long its not marked for close
	//
	virtual uint32 Run()
	{
		while (!bexit && _cmp->ConnectionListenSocket) {
			_cmp->Run_ConnectionListener();

			FPlatformProcess::Sleep(_cmp->ListenTickDuration);
		}
		return (0);
	}
	// Run()
};
// FTCPListenThread


// ================================ //
//       AMatchmakingWebPanel       //
// ================================ //

// Constructor()
//
// Task: Sets default values
//
AMatchmakingWebPanel::AMatchmakingWebPanel()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set Default values
	ListenTickDuration = 0.03f;
	ConnectionAcceptDelay = 0.05f;
	ListenPort = 333;
	ShowDebugData = false;
	FilePathRoot = TEXT("C:/matchmaking/webpanel");
}
// Constructor()



// BeginPlay()
//
// Task: Start the ConnectionListener
// 
void AMatchmakingWebPanel::BeginPlay()
{
	Super::BeginPlay();

	// Dont run in editor
	if(UMatchmakingBPLibrary::Matchmaking_RunningInEditor() == false)
		Start_ConnectionListener();
}
// Constructor()



// EndPlay()
//
// Task: Prepare for game end, close all sockets and destroy them
//
void AMatchmakingWebPanel::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);


	// Dont run in editor
	if (UMatchmakingBPLibrary::Matchmaking_RunningInEditor() == false) {

		// Stop threads
		if (ConnectionListenThread) {
			ConnectionListenThread->Stop();
			delete ConnectionListenThread;
			ConnectionListenThread = nullptr;
		}

		// Close Sockets
		if (ConnectionListenSocket) {
			ConnectionListenSocket->Close();
			ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ConnectionListenSocket);
		}

		if (ConnectionReceiveSocket) {
			ConnectionReceiveSocket->Close();
			ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ConnectionReceiveSocket);
		}
	}
}
// EndPlay()



// Tick()
//
// Task: 
//
void AMatchmakingWebPanel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
// Tick()



// Start_ConnectionListener()
//
// Task: Create the Listen Socket and the thread
// 
void AMatchmakingWebPanel::Start_ConnectionListener() {

	// Create the new socket
	FIPv4Endpoint Endpoint(FIPv4Address(127, 0, 0, 1), ListenPort);
	ConnectionListenSocket = FTcpSocketBuilder(TEXT("TCPConnectSocket")).AsReusable().BoundToEndpoint(Endpoint).Listening(8);

	// Set Buffer Size
	int32 NewSize = 0;
	int32 BufferSize = 2 * 1024 * 1024;
	ConnectionListenSocket->SetReceiveBufferSize(BufferSize, NewSize);

	// Output information about started Listener
	if (ShowDebugData)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("TCP Listener started"));


	// Create the ListenThread
	if (ConnectionListenSocket) {
		ConnectionListenThread = new FTCPListenThread(this);
	}
}
// Start_ConnectionListener()



// Run_ConnectionListener()
//
// Task: check for incoming connections and start the Receiver if needed
//
void AMatchmakingWebPanel::Run_ConnectionListener() {

	// Make sure the Socket is valid
	if (!ConnectionListenSocket) return;

	// Prepare for Remote Address
	TSharedRef<FInternetAddr> RemoteAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	bool HasPendingConnection;

	// check for incoming connections
	if (ConnectionListenSocket->HasPendingConnection(HasPendingConnection) && HasPendingConnection) {

		// Clear the socket in case it already exists
		if (ConnectionReceiveSocket) {
			ConnectionReceiveSocket->Close();
			ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(ConnectionReceiveSocket);
		}

		// Sleep quick to make sure the incoming connection is really available
		FPlatformProcess::Sleep(ConnectionAcceptDelay);

		// Accept new connection
		ConnectionReceiveSocket = ConnectionListenSocket->Accept(*RemoteAddress, TEXT("TCP Connection Accepted"));

		// Output that we have something incoming!
		if (ShowDebugData)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Connection Incoming"));

		// Check if Receive Socket is valid, then get the Remote Address and start receiving
		if (ConnectionReceiveSocket != NULL) {
			FIPv4Endpoint tmp_RemoteAddress = FIPv4Endpoint(RemoteAddress);

			Run_ConnectionReceiver(ConnectionReceiveSocket);
		}
	}

}
// Run_ConnectionListener()



// BinToString()
//
// Task: Convert a Binary to String
//
FString AMatchmakingWebPanel::BinToString(TArray<uint8> BinArr)
{
	BinArr.Add(0); // Add 0 termination. Even if the string is already 0-terminated, just to be sure
	return FString(ANSI_TO_TCHAR(reinterpret_cast<const char*>(BinArr.GetData())));
}
// BinToString()



// StringToBin()
//
// Task: Convert String to Binary
//
// 
TArray<uint8> AMatchmakingWebPanel::StringToBin(const FString& str)
{
	TArray<uint8> BinData;
	FTCHARToUTF8 UStr(*str);
	BinData.Append((uint8*)UStr.Get(), UStr.Length());
	return BinData;
}
// StringToBin()



// ConvertASCII()
//
// Task: Convert ASCII characters
//
// 
FString AMatchmakingWebPanel::ConvertASCII(const FString str)
{
	FString tmp_Str = str;

	tmp_Str = tmp_Str.Replace(TEXT("+"), TEXT(" "));
	tmp_Str = tmp_Str.Replace(TEXT("%20"), TEXT(" "));
	tmp_Str = tmp_Str.Replace(TEXT("%21"), TEXT("!"));
	tmp_Str = tmp_Str.Replace(TEXT("%22"), TEXT("'"));
	tmp_Str = tmp_Str.Replace(TEXT("%23"), TEXT("#"));
	tmp_Str = tmp_Str.Replace(TEXT("%24"), TEXT("$"));
	tmp_Str = tmp_Str.Replace(TEXT("%25"), TEXT("%"));
	tmp_Str = tmp_Str.Replace(TEXT("%26"), TEXT("&"));
	tmp_Str = tmp_Str.Replace(TEXT("%27"), TEXT("'"));
	tmp_Str = tmp_Str.Replace(TEXT("%28"), TEXT("("));
	tmp_Str = tmp_Str.Replace(TEXT("%29"), TEXT(")"));
	tmp_Str = tmp_Str.Replace(TEXT("%2A"), TEXT("!"));
	tmp_Str = tmp_Str.Replace(TEXT("%2B"), TEXT("+"));
	tmp_Str = tmp_Str.Replace(TEXT("%2C"), TEXT(","));
	tmp_Str = tmp_Str.Replace(TEXT("%2D"), TEXT("-"));
	tmp_Str = tmp_Str.Replace(TEXT("%2E"), TEXT("."));
	tmp_Str = tmp_Str.Replace(TEXT("%2F"), TEXT("/"));
	tmp_Str = tmp_Str.Replace(TEXT("%3A"), TEXT(":"));
	tmp_Str = tmp_Str.Replace(TEXT("%3B"), TEXT(";"));
	tmp_Str = tmp_Str.Replace(TEXT("%3C"), TEXT("<"));
	tmp_Str = tmp_Str.Replace(TEXT("%3D"), TEXT("="));
	tmp_Str = tmp_Str.Replace(TEXT("%3E"), TEXT(">"));
	tmp_Str = tmp_Str.Replace(TEXT("%3F"), TEXT("?"));
	tmp_Str = tmp_Str.Replace(TEXT("%40"), TEXT("@"));
	tmp_Str = tmp_Str.Replace(TEXT("%5B"), TEXT("]"));
	tmp_Str = tmp_Str.Replace(TEXT("%5C"), TEXT("/"));
	tmp_Str = tmp_Str.Replace(TEXT("%5D"), TEXT("]"));
	tmp_Str = tmp_Str.Replace(TEXT("%5E"), TEXT("^"));
	tmp_Str = tmp_Str.Replace(TEXT("%5F"), TEXT("_"));
	tmp_Str = tmp_Str.Replace(TEXT("%60"), TEXT("`"));
	tmp_Str = tmp_Str.Replace(TEXT("%7B"), TEXT("{"));
	tmp_Str = tmp_Str.Replace(TEXT("%7C"), TEXT("|"));
	tmp_Str = tmp_Str.Replace(TEXT("%7D"), TEXT("}"));
	tmp_Str = tmp_Str.Replace(TEXT("%7E"), TEXT("~"));
	tmp_Str = tmp_Str.Replace(TEXT("%80"), TEXT("�"));

	return tmp_Str;
}
// ConvertASCII()



// Run_ConnectionReceiver()
//
// Task: Receive the actual Message
//
void AMatchmakingWebPanel::Run_ConnectionReceiver(FSocket* ReceiveSocket) {

	// In case the ReceiveSocket is invalid -> Stop the thread
	if (!ReceiveSocket) {
		return;
	}

	TArray<uint8> ReceivedData;
	uint32 Size;

	// Receive Data as long as there is something to receive
	if (ReceiveSocket->HasPendingData(Size)) {
		ReceivedData.Init(FMath::Min(Size, 65507u), Size);

		// Receive actual Data
		int32 Read = 0;
		ReceiveSocket->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);

		// Only proceed if given Data is not empty
		if (ReceivedData.Num() <= 0) return;

		// Output the byte amount!
		if (ShowDebugData)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Data Bytes Read -> %d"), ReceivedData.Num()));

		// Convert Received Data to String
		const FString ReceivedString = BinToString(ReceivedData);

		// Outbut the String Data
		if (ShowDebugData)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("As String Data ~> %s"), *ReceivedString));

		// Get the request String (Path to requested file)
		bool RequestSuccess = GetRequestString(ReceivedString);

		// Load File content
		FString fileContent = "";
		FString tmp_FilePathRoot = FilePathRoot;
		FString filePath = tmp_FilePathRoot.Append(Response_Data);
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

		// Needed for sending the response
		int32 tmp_BytesSent = 0;

		// Prepare Headers
		FString HTTPHeader_P1 = TEXT("HTTP/1.1 200 OK\r\nServer: Matchmaking Server (By Robin Zinser)\r\nContent-Length: ");
		FString HTTPHeader_P2 = TEXT("\r\nContent-Language: de\r\nConnection: close\r\nContent-Type: ");
		FString HTTPHeader_P3 = TEXT("\r\n\r\n");

		// Respond if the requested file exists
		if (PlatformFile.FileExists(*filePath) && RequestSuccess) {

			FFileHelper::LoadFileToString(fileContent, *filePath);
			fileContent = ProcessRequestParameters(fileContent, Response_Parameters, Response_GetPost);
			FString MIMEType = GetMIMEType(filePath);
			FString HTTPResponse = HTTPHeader_P1;
			HTTPResponse.AppendInt(fileContent.Len());
			HTTPResponse.Append(HTTPHeader_P2);
			HTTPResponse.Append(MIMEType);
			HTTPResponse.Append(HTTPHeader_P3);
			HTTPResponse.Append(fileContent);

			// Send Response
			const TArray<uint8>& data = StringToBin(HTTPResponse);
			ReceiveSocket->Send(data.GetData(), data.Num(), tmp_BytesSent);
		}

		// Process UE4Connector
		else if (Response_Data.Find(TEXT("UE4Connector")) > -1) {

			fileContent = TEXT(""); // Clear contents because there is no actual file for this request
			fileContent = ProcessRequestParameters(fileContent, Response_Parameters, Response_GetPost);
			FString HTTPResponse = HTTPHeader_P1;
			HTTPResponse.AppendInt(fileContent.Len());
			HTTPResponse.Append(HTTPHeader_P2);
			HTTPResponse.Append(TEXT("text/html"));
			HTTPResponse.Append(HTTPHeader_P3);
			HTTPResponse.Append(fileContent);

			// Send Response
			const TArray<uint8>& data = StringToBin(HTTPResponse);
			ReceiveSocket->Send(data.GetData(), data.Num(), tmp_BytesSent);
		}

		// Bad Request -> Redirect to index.html
		else {
			tmp_FilePathRoot = FilePathRoot;
			filePath = tmp_FilePathRoot.Append("/index.html");

			// Send index.html if exists, otherwise send 404
			if (PlatformFile.FileExists(*filePath)) {
				FFileHelper::LoadFileToString(fileContent, *filePath);
				fileContent = ProcessRequestParameters(fileContent, Response_Parameters, Response_GetPost);
				FString MIMEType = GetMIMEType(filePath);
				FString HTTPResponse = HTTPHeader_P1;
				HTTPResponse.AppendInt(fileContent.Len());
				HTTPResponse.Append(HTTPHeader_P2);
				HTTPResponse.Append(MIMEType);
				HTTPResponse.Append(HTTPHeader_P3);
				HTTPResponse.Append(fileContent);

				// Send Response
				const TArray<uint8>& data = StringToBin(HTTPResponse);
				ReceiveSocket->Send(data.GetData(), data.Num(), tmp_BytesSent);

				// Show debug MSG
				if (ShowDebugData)
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Redirected to index.html")));
			}

			else {
				const TArray<uint8>& data = StringToBin(TEXT("HTTP/1.1 404\r\nServer: Apache/1.3.29 (Unix) PHP/4.3.4\r\nContent-Length: 22\r\nContent-Language: de\r\nConnection: close\r\nContent-Type: text/html\r\n\r\nInternal Server Error!"));
				ReceiveSocket->Send(data.GetData(), data.Num(), tmp_BytesSent);

				// Show debug MSG
				if (ShowDebugData)
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("CANT FIND FILE: %s"), *filePath));
			}
		}

	}
}
// Run_ConnectionReceiver()



// GetRequestString()
//
// Task: Extract the Request string, the raw parameters and the RequestType (Get/Post)
//
bool AMatchmakingWebPanel::GetRequestString(const FString StrData) {

	// Get the actual request part
	FString tmp_Data = StrData.RightChop(4);
	tmp_Data = tmp_Data.Left(tmp_Data.Find("HTTP/1.1") - 1);

	// Is this a GET request?
	if (StrData.Left(3) == "GET") {

		// Look for parameters
		int32 tmp_HasParameters = tmp_Data.Find("?");

		// Prepare variables
		TMap<FString, FString> tmp_Parameters;
		FString tmp_ParameterString;

		// There are parameters available
		if (tmp_HasParameters > -1) {

			TArray<FString> tmp_Parameters_HelperArray;

			tmp_ParameterString = tmp_Data.RightChop(tmp_HasParameters + 1);
			tmp_ParameterString.ParseIntoArray(tmp_Parameters_HelperArray, TEXT("&"), true);

			// Clean request file path
			tmp_Data = tmp_Data.Left(tmp_HasParameters);

			// Show debug MSG
			if (ShowDebugData)
				GEngine->AddOnScreenDebugMessage(-1, 3.5f, FColor::Green, TEXT("Reading GET Parameters"));

			while (tmp_Parameters_HelperArray.Num() > 0) {
				FString Par_Name;
				FString Par_Val;
				tmp_Parameters_HelperArray[0].Split(TEXT("="), &Par_Name, &Par_Val, ESearchCase::IgnoreCase);
				tmp_Parameters_HelperArray.RemoveAt(0);

				// ASCII Convertion
				Par_Name = ConvertASCII(Par_Name);
				Par_Val = ConvertASCII(Par_Val);

				// Show debug MSG
				if (ShowDebugData) {
					FString LogStr = Par_Name;
					LogStr = LogStr.Append(TEXT(" ->> ")).Append(Par_Val);
					GEngine->AddOnScreenDebugMessage(-1, 3.5f, FColor::Green, LogStr);
				}

				// Add Parameter entry to Map
				tmp_Parameters.Add(Par_Name, Par_Val);
			}
		}

		// Write Response Data
		Response_GetPost = false;
		Response_Data = tmp_Data;
		Response_Parameters = tmp_Parameters;

		return true;
	}

	// Is this a POST request?
	else if (StrData.Left(4) == "POST") {

		// Remove another char from the data string since "POST" has one more letter than "GET"
		tmp_Data = tmp_Data.RightChop(1);

		// Create needed Vars
		TMap<FString, FString> tmp_Parameters;
		TArray<FString> tmp_Parameters_HelperArray;
		FString tmp_ParameterString = StrData.RightChop(StrData.Find("\r\n\r\n") + 4);

		// Look for parameters
		int32 tmp_HasParameters = tmp_ParameterString.Find("=");


		// There are parameters available
		if (tmp_HasParameters > -1) {

			// Split parameter+value sets into array
			tmp_ParameterString.ParseIntoArray(tmp_Parameters_HelperArray, TEXT("&"), true);

			// Show debug MSG
			if (ShowDebugData)
				GEngine->AddOnScreenDebugMessage(-1, 3.5f, FColor::Green, TEXT("Reading POST Parameters"));

			// Loop through the array and sperate ParName and Val and add it to the map
			while (tmp_Parameters_HelperArray.Num() > 0) {
				FString Par_Name;
				FString Par_Val;
				tmp_Parameters_HelperArray[0].Split(TEXT("="), &Par_Name, &Par_Val, ESearchCase::IgnoreCase);
				tmp_Parameters_HelperArray.RemoveAt(0);

				// ASCII Convertion
				Par_Name = ConvertASCII(Par_Name);
				Par_Val = ConvertASCII(Par_Val);

				// Show debug MSG
				if (ShowDebugData) {
					FString LogStr = Par_Name;
					LogStr = LogStr.Append(TEXT(" ->> ")).Append(Par_Val);

					GEngine->AddOnScreenDebugMessage(-1, 3.5f, FColor::Green, LogStr);
				}

				// Add Parameter entry to Map
				tmp_Parameters.Add(Par_Name, Par_Val);
			}
		}

		// Write Response Data
		Response_GetPost = true;
		Response_Data = tmp_Data;
		Response_Parameters = tmp_Parameters;

		return true;
	}

	// -> No proper request
	else {
		// Show debug MSG
		if (ShowDebugData)
			GEngine->AddOnScreenDebugMessage(-1, 3.5f, FColor::Red, TEXT("NO REQUEST FOUND"));

		return false;
	}
}
// GetRequestString()


// ProcessRequestParameters()
//
// Task: Accessible through Blueprints to work with the Parameters
//
FString AMatchmakingWebPanel::ProcessRequestParameters_Implementation(const FString& RequestFileContents, const TMap<FString, FString>& Parameters, bool GetPost) {
	return RequestFileContents;
}
// ProcessRequestParameters()



// GetMIMEType()
//
// Task: Gets the MIME-Type for the given Filepath
//
FString AMatchmakingWebPanel::GetMIMEType(const FString FilePath) {
	if (FilePath.Find(".js", ESearchCase::IgnoreCase) > -1)
		return "text/javascript";
	else if (FilePath.Find(".css", ESearchCase::IgnoreCase) > -1)
		return "text/css";
	else if (FilePath.Find(".xml", ESearchCase::IgnoreCase) > -1)
		return "text/xml";
	else if (FilePath.Find(".jpg", ESearchCase::IgnoreCase) > -1)
		return "image/jpeg";
	else if (FilePath.Find(".jpe", ESearchCase::IgnoreCase) > -1)
		return "image/jpeg";
	else if (FilePath.Find(".jpeg", ESearchCase::IgnoreCase) > -1)
		return "image/jpeg";
	else if (FilePath.Find(".png", ESearchCase::IgnoreCase) > -1)
		return "image/png";
	else if (FilePath.Find(".gif", ESearchCase::IgnoreCase) > -1)
		return "image/gif";
	else if (FilePath.Find(".tif", ESearchCase::IgnoreCase) > -1)
		return "image/tiff";
	else if (FilePath.Find(".tiff", ESearchCase::IgnoreCase) > -1)
		return "image/tiff";
	else if (FilePath.Find(".bmp", ESearchCase::IgnoreCase) > -1)
		return "image/bmp";
	else
		return "text/html";
}
// GetMIMEType()



// GetMD5Sum()
//
// Task: Hash the given String using MD5
//
FString AMatchmakingWebPanel::GetMD5Sum(FString InputText)
{
	return FMD5::HashAnsiString(*InputText);
}
// GetMD5Sum()



// ValidatePath()
//
// Task: check if given Path is valid
//
bool AMatchmakingWebPanel::ValidatePath(FString Path)
{
	return (FPlatformFileManager::Get().GetPlatformFile().FileExists(*Path));
}
// ValidatePath()