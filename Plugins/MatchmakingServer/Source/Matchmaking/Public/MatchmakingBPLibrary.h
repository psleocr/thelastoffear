// Copyright 2017-2018 Robin Zinser. All Rights Reserved.

#pragma once

#include "Runtime/Core/Public/GenericPlatform/GenericPlatformProcess.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Engine.h"
#include "MatchmakingBPLibrary.generated.h"

/* 
*	Function library class.
*	Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.
*
*	When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.
*	BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.
*	BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.
*	DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.
*				Its lets you name the node using characters not allowed in C++ function names.
*	CompactNodeTitle - the word(s) that appear on the node.
*	Keywords -	the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu. 
*				Good example is "Print String" node which you can find also by using keyword "log".
*	Category -	the category your node will be under in the Blueprint drop-down menu.
*
*	For more info on custom blueprint nodes visit documentation:
*	https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation
*/
UCLASS()
class UMatchmakingBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()
		
	// Start a Dedicated Server on given Path with given Map
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Start Dedicated Server", Keywords = "Matchmaking Start Dedicated Server"), Category = "Matchmaking")
	static void Matchmaking_Server_Start(FString ServerPath, FString Map);

	// Start a Dedicated Server on given Path with given Map
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Travel to Dedicated Server", Keywords = "Matchmaking Travel to Dedicated Server"), Category = "Matchmaking")
	static void Matchmaking_Client_Travel(APlayerController* PlayerController, FString Adress);

	// Get (if this is the Server) the Port the Server is running on
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get used Server Port", Keywords = "Matchmaking Get used Server Port"), Category = "Matchmaking")
	static int Matchmaking_Server_GetPort(AActor* SourceActor);
	
	// return True if the Project is running in the Editor
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Is running in Editor", Keywords = "Matchmaking running in Editor"), Category = "Matchmaking")
	static bool Matchmaking_RunningInEditor();

	// Shut down the server (used for dedicated servers)
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Shut down Dedicated Server", Keywords = "Matchmaking Dedicated server shutdown"), Category = "Matchmaking")
	static void Matchmaking_DedicatedServer_ShutDown();

};
