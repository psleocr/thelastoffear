// Copyright 2017-2018 Robin Zinser. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "MatchmakingBPLibrary.h"
#include "Runtime/Core/Public/Misc/Base64.h"
#include "Runtime/Sockets/Public/Sockets.h"
#include "Runtime/Sockets/Public/SocketSubsystem.h"
#include "Runtime/Sockets/Public/IPAddress.h"
#include "Runtime/Networking/Public/Interfaces/IPv4/IPv4Endpoint.h"
#include "Runtime/Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "Runtime/Networking/Public/Common/TcpSocketBuilder.h"
#include "Runtime/Core/Public/Misc/SecureHash.h"
#include "MatchmakingWebPanel.generated.h"


/**
* WebPanel Actor for Matchmaking
*/
UCLASS(hideCategories = (Tags, Activation, Cooking, Collision), ClassGroup = Matchmaking, meta = (BlueprintSpawnableComponent))
class MATCHMAKING_API AMatchmakingWebPanel : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMatchmakingWebPanel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	friend class FTCPListenThread;

	FSocket* ConnectionListenSocket;
	FSocket* ConnectionReceiveSocket;
	class FRunnable* ConnectionListenThread;

	// HTTPResponseData
	FString Response_Data;
	bool Response_GetPost; // 0 = GET // 1 = POST
	TMap<FString, FString> Response_Parameters;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Start_ConnectionListener();
	void Run_ConnectionListener();
	void Run_ConnectionReceiver(FSocket* ReceiveSocket);
	bool GetRequestString(const FString StrData);
	FString GetMIMEType(const FString FilePath);
	FString BinToString(TArray<uint8> BinArr);
	TArray<uint8> StringToBin(const FString& str);
	FString ConvertASCII(const FString str);

	UFUNCTION(BlueprintCallable, Category = "Matchmaking WebPanel")
		FString GetMD5Sum(FString InputText);

	UFUNCTION(BlueprintCallable, Category = "Matchmaking WebPanel")
		bool ValidatePath(FString Path);

	// Public Vars
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
		float ListenTickDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
		float ConnectionAcceptDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
		int32 ListenPort;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
		bool ShowDebugData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
		FString FilePathRoot;

	// BP Event for Parameters
	UFUNCTION(BlueprintNativeEvent, Category = "Matchmaking WebPanel")
		FString ProcessRequestParameters(const FString& RequestFileContents, const TMap<FString, FString>& Parameters, bool GetPost);
	virtual FString ProcessRequestParameters_Implementation(const FString& RequestFileContents, const TMap<FString, FString>& Parameters, bool GetPost);

};